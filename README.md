# Decision Tree #

Decision tree for classification. 
### Features: ###
+ ID3 algorithm
+ Chain of equality or inequality rules in nodes.
+ Pre-pruning
+ Different information criterion: fisher exact criterion, entropy or you can provide your own.
+ Prediction function with ability to estimate quality on test set.
+ Test stand with different dataset
+ Nice print of tree in txt format

### Result of algorithm on different data sets: ###
+ [Congressman identification problem](http://archive.ics.uci.edu/ml/datasets/Congressional+Voting+Records) with data sampling during 100 iterations:
	+ Fisher exact criterion results on average:
		+ accurancy 0.898
		+ truly classified 20.67
		+ falsely classified 2.33
	+ Entropy criterion results:
		+ accurancy 0.917
		+ truly classified 21.1
		+ falsely classified 1.9
+ [Wheat identification problem](http://archive.ics.uci.edu/ml/datasets/Congressional+Voting+Records)
	+ Fisher exact criterion result:
		+ accurancy 0.904
		+ truly classified 19
		+ falsely classified 2
	+ Entropy criterion result:
		+ accurancy 0.952
		+ truly classified 20
		+ falsely classified 1
+ [Mushroom poisonousness problem](http://archive.ics.uci.edu/ml/datasets/Mushroom) with data sampling during 100 iterations:
	+ Fisher exact criterion result:
		+ accurancy 0.766
		+ truly classified 622.54
		+ falsely classified 189.46
	+ Entropy criterion result:
		+ accurancy 0.783
		+ truly classified 604.11
		+ falsely classified 207.89

Tree example for [seed dataset](http://archive.ics.uci.edu/ml/datasets/seeds):
![tree](https://bytebucket.org/tronok/decisiontree/raw/e460a4b22dfa5d12d62c8455da4393e2f2af988e/tree_example.jpg)